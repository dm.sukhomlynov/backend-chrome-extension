## Chrome extension app api

Backend api application based on Laravel 8 using JWT

## Server Requirements

The Laravel framework has a few system requirements. You should ensure that your web server has the following minimum PHP version and extensions:

- **PHP >= 7.3**
- **BCMath PHP Extension**
- **Ctype PHP Extension**
- **Fileinfo PHP Extension**
- **JSON PHP Extension**
- **Mbstring PHP Extension**
- **OpenSSL PHP Extension**
- **PDO PHP Extension**
- **Tokenizer PHP Extension**
- **XML PHP Extension**

## Installing

    1) Download the repository
    2) Execute the command to deploy: composer install --optimize-autoloader --no-dev
    3) Execute the command: php artisan config:cache
    4) Rename .env.example to .env
    5) Create a database
    6) In file .env edit:
        DB_DATABASE=your_data_base
        DB_USERNAME=username
        DB_PASSWORD=password
    7) Execute the command: php artisan key:generate
    8) Execute the command: php artisan cache:clear && php artisan config:cache
    9) Perform migrations: php artisan migrate
    10) Execute the command:: php artisan jwt:secret
    11) Execute the command: php artisan cache:clear && php artisan config:cache

## Available api

- GET /uid - returns a unique identifier (uid)
- POST /user - Creates a user, takes a JSON object with data:
{
    "uid": randomly_generated_string,
    "login": string,
    "pass": string,
    "chrome-identity": {
        "email": string,
        "gaia": string
    }
}
chrome-identity may be missing or incomplete
- POST /authorize - Authorizes user by login-password, returns JWT
- * POST /key/:name - Stores in the database the key named name associated with the user
- * GET /key/:name - Returns the user associated key named name or null from the database
- * GET /keys - Returns all the keys associated with the user from the database

All endpoints marked with asterisk * require JWT (Authorization header)

## Server Responses

The answer always comes in format:


    - GET /uid:    
    {
        "message": "uid successfully created",
        "uid": "d0b8e502b24a7d9a1c4c4610b157f9be"
    }
    
    - POST /user:
    {
        "message": "User successfully registered",
        "user": {
            "uid": "2d73816c90928f826a11145c8b0f58fd",
            "login": "login",
            "email": "test@gmail.com1",
            "gaia": "gaiaString",
            "updated_at": "2021-06-08T23:07:51.000000Z",
            "created_at": "2021-06-08T23:07:51.000000Z",
            "id": 1
        }
    }
    
    - POST /authorize:
    {
        "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC9hdXRob3JpemUiLCJpYXQiOjE2MjMxOTM3MTIsImV4cCI6MTYyMzE5NzMxMiwibmJmIjoxNjIzMTkzNzEyLCJqdGkiOiJ3S1k3QkpOYVNTR2YzcGxQIiwic3ViIjoxLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.nyWnk_GfGelMdH5tBOFGBXLHbZ12pmnFPVCssVwGrXs",
        "token_type": "bearer",
        "expires_in": 3600,
        "user": {
            "id": 1,
            "uid": "2d73816c90928f826a11145c8b0f58fd",
            "login": "login",
            "email": "test@gmail.com1",
            "email_verified_at": null,
            "gaia": "gaiaString",
            "created_at": "2021-06-08T23:07:51.000000Z",
            "updated_at": "2021-06-08T23:07:51.000000Z"
        }
    }
    
    - * POST /key/:name:
    {
        "message": "Key successfully added",
        "user_id": 1,
        "key": "dfsdffffffffffff",
        "name": "55"
    }
    
    - * GET /key/:name:
    {
        "message": "return key_value",
        "key_value": "key1"
    }
    
    - * GET /keys:
    {
        "message": "return key_values",
        "key_values": [
            "key1",
            "key2"
        ]
    }

