<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Keys;
use Validator;


class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['getUser', 'login', 'register']]);
    }

    /**
     * Get a new User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUser(){
        $uid = md5(uniqid(rand(), true));

        return response()->json([
            'message' => 'uid successfully created',
            'uid' => $uid
        ], 200);
    }

    /**
     * Register a User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request) {
        // Input data preprocessing
        $RequestData = $request->all();
        if (array_key_exists('chrome-identity', $RequestData)) {
            $chromeIdentity = $RequestData['chrome-identity'];
            if($chromeIdentity['email']){
                $RequestData['email'] = trim($chromeIdentity['email']);
            }
            if($chromeIdentity['gaia']){
                $RequestData['gaia'] = trim($chromeIdentity['gaia']);
            }
        }
        if($RequestData['pass']){
            $password =  trim($RequestData['pass']);
        }
        unset($RequestData['chrome-identity']);
        $RequestData['uid'] = md5(uniqid(rand(), true));
        //End Input data preprocessing

        $validator = Validator::make($RequestData, [
            'uid' => 'string',
            'login' => 'required|string|between:2,100|unique:users',
            'pass' => 'required|string|min:6',
            'email' => 'string|email|max:100|unique:users',
            'gaia' => 'string',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = User::create(array_merge(
            $validator->validated(),
            ['password' => bcrypt($password)]
        ));

        return response()->json([
            'message' => 'User successfully registered',
            'user' => $user
        ], 201);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request){
        // Input data preprocessing
        $RequestData = $request->all();
        if($RequestData['pass']){
            $RequestData['password'] = trim($RequestData['pass']);
            $RequestData['pass'] = '';
        }
        unset($RequestData['pass']);
        //End Input data preprocessing

        $validator = Validator::make($RequestData, [
            'login' => 'required',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        if (! $token = auth()->attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->createNewToken($token);
    }

    /**
     * Set key User in DataBase
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function setKey(Request $request, $name){
        // Input data preprocessing
        $UserId = auth()->user()->id;
        $key_value = trim($request->getContent());
        //End Input data preprocessing

        if(!$name){
            return response()->json('The name was not found', 500);
        }
        if(!$key_value){
            return response()->json('The key was not found', 500);
        }

        $keyInDB = DB::table('keys')
                ->where('user_id', '=', "$UserId")
                ->where('key_name', '=', "$name")
                ->first();

        if($keyInDB){
            $id = $keyInDB->id;
            $keyObj = Keys::find($id);
            $keyObj->key_value = $key_value;
            $text = 'changed';
        }else{
            $keyObj = new Keys;
            $keyObj->user_id = $UserId;
            $keyObj->key_name = $name;
            $keyObj->key_value = $key_value;
            $text = 'added';
        }

        if($keyObj->save()){
            return response()->json([
                'message' => "Key successfully $text",
                'user_id' => $UserId,
                'key' => $key_value,
                'name' => $name
            ], 201);
        }else{
            return response()->json('The key was not added', 500);
        }
    }

    /**
     * Get key User from table Keys
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getKey(Request $request, $name){
        // Input data preprocessing
        $UserId = auth()->user()->id;
        //End Input data preprocessing

        $keyInDB = DB::table('keys')
            ->where('user_id', '=', "$UserId")
            ->where('key_name', '=', "$name")
            ->first();

        if($keyInDB){
            $key_value = $keyInDB->key_value;
            return response()->json([
                'message' => 'return key_value',
                'key_value' => $key_value
            ], 200);
        }else{
            return response()->json([
                'message' => 'key_value was not found',
                'key_value' => NULL
            ], 500);
        }
    }

    /**
     * Get keys User from table Keys
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getKeys(Request $request){
        // Input data preprocessing
        $UserId = auth()->user()->id;
        //End Input data preprocessing

        $keysInDB = DB::table('keys')
            ->where('user_id', '=', "$UserId")
            ->get();

        if($keysInDB){
            $keysArr = [];
            foreach($keysInDB as $keyInDB){
                $keysArr []= $keyInDB->key_value;
            }
            return response()->json([
                'message' => 'return key_values',
                'key_values' => $keysArr
            ], 200);
        }else{
            return response()->json([
                'message' => 'key_value was not found',
                'key_values' => NULL
            ], 500);
        }
    }


    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {
        auth()->logout();

        return response()->json(['message' => 'User successfully signed out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {
        return $this->createNewToken(auth()->refresh());
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile() {
        return response()->json(auth()->user());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token){
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }

}