<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => ''

], function ($router) {
    Route::get('/uid', [AuthController::class, 'getUser']);
    Route::post('/user', [AuthController::class, 'register']);
    Route::post('/authorize', [AuthController::class, 'login']);
    Route::post('/key/{name}', [AuthController::class, 'setKey']);
    Route::get('/key/{name}', [AuthController::class, 'getKey']);
    Route::get('/keys', [AuthController::class, 'getKeys']);

    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/user-profile', [AuthController::class, 'userProfile']);
});